# Build stage
FROM maven:3.8.1-jdk-11-slim
COPY src /home/app/src
COPY pom.xml /home/app
RUN mvn -f /home/app/pom.xml clean compile package -Dv-1.0.0-test

